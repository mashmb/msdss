package software.ternary.tss.ms2.infrastructure;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.interceptor.InternalAuthInterceptor;

/**
 * Configuration for system internal services.
 *
 * @author TSS
 */
@Slf4j
@Configuration
class InternalServicesConfig {

  @Value("${security.authorization-server}")
  private String authServerUrl;

  @Value("${security.internal.login}")
  private String internalLogin;

  @Value("${security.internal.password}")
  private String internalPassword;

  @Value("${security.jwt.client.id}")
  private String jwtClientId;

  @Value("${security.jwt.client.secret}")
  private String jwtClientSecret;

  @Value("${security.internal.login-attempts}")
  private int loginAttempts;

  @Value("${security.jwt.token.endpoint.token}")
  private String tokenResource;

  /**
   * Configure rest template used to communicate with internal services in secure
   * way (handle OAuth 2).
   *
   * @return RestTemplate configured for secured internal communication rest
   *         template
   */
  @Bean
  public RestTemplate ineternalRestTemplate() {
    log.debug("Configuring internal rest template");
    RestTemplate restTemplate = new RestTemplate();
    List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();

    if (interceptors == null) {
      interceptors = new ArrayList<>();
    }

    interceptors.add(new InternalAuthInterceptor(loginAttempts, authServerUrl, jwtClientId, jwtClientSecret,
        tokenResource, internalLogin, internalPassword));
    restTemplate.setInterceptors(interceptors);
    log.debug("Internal rest template configured [restTemplate = {}]", restTemplate);

    return restTemplate;
  }

}
