package software.ternary.tss.ms2.port.input.sample;

import org.springframework.http.ResponseEntity;

import software.ternary.tss.ms2.core.domain.dto.HelloMessageDto;

/**
 * Definition of hello world controller.
 *
 * @author TSS
 */
public interface HelloWorldController {

  /**
   * Message for authorized user.
   * 
   * @return ResponseEntity HTTP response with message
   */
  ResponseEntity<HelloMessageDto> getPrivateMessage();

}
