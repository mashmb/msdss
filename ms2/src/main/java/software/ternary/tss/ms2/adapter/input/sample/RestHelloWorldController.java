package software.ternary.tss.ms2.adapter.input.sample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.ms2.core.domain.dto.HelloMessageDto;
import software.ternary.tss.ms2.port.input.sample.HelloWorldController;

/**
 * Rest implementation of hello world controller.
 *
 * @author TSS
 */
@Slf4j
@RestController
class RestHelloWorldController implements HelloWorldController {

  private final RestTemplate restTemplate;

  @Value("${system.internal.microservices.ms1}")
  private String ms1Url;

  /**
   * RestHelloWorldController constructor.
   *
   * @param restTemplate access to internal rest template
   */
  public RestHelloWorldController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  /**
   * Message for authorized user.
   * 
   * @return ResponseEntity HTTP response with message
   */
  @Override
  @RequestMapping(path = "/hello/private", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloMessageDto> getPrivateMessage() {
    log.debug("Start of HelloWorldController.getPrivateMessage [path = {}]", "/hello/private");
    HelloMessageDto dto = null;

    try {
      ResponseEntity<HelloMessageDto> response = restTemplate.exchange(ms1Url + "/hello/private", HttpMethod.GET, null,
          HelloMessageDto.class);
      dto = response.getBody();
    } catch (Exception ex) {
      log.error("Internal HTTP request failed", ex);
    }

    log.debug("End of HelloWorldController.getPrivateMessage [path = {}]", "/hello/private");

    return ResponseEntity.status(200).body(dto);
  }

}
