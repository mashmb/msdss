package software.ternary.tss.ms2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Second example microservice main entry point.
 *
 * @author TSS
 */
@SpringBootApplication
public class Ms2Application {

  /**
   * Application main entry point.
   *
   * @param args application arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(Ms2Application.class, args);
  }

}
