#!/bin/bash
# 
# Application development build script.
# 
# @author TSS

./gradlew clean
./gradlew build
java -jar build/libs/ms2-1.0.0.jar --server.port=8081 --spring.profiles.active=auth-server-client
