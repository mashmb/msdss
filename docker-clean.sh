#!/bin/bash
#
# Multi module project Docker clean script.
#
# @author TSS

docker container rm -f $(docker ps -aq --filter="name=authserver")
docker container rm -f $(docker ps -aq --filter="name=ms1")
docker container rm -f $(docker ps -aq --filter="name=ms2")
docker container rm -f $(docker ps -aq --filter="name=database")

docker image rm -f authserver:dev
docker image rm -f ms1:dev
docker image rm -f ms2:dev
