package software.ternary.tss.seclib.interceptor;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.util.JwtUtils;

/**
 * Interceptor used to provide authentication between secured micro services
 * (internal networking).
 *
 * @author TSS
 */
@Slf4j
public class InternalAuthInterceptor implements ClientHttpRequestInterceptor {

  private final int attempts;
  private final String authServerUrl;
  private final String jwtClientId;
  private final String jwtClientSecret;
  private final ObjectMapper objectMapper;
  private final String tokenResource;
  private final String username;
  private final String password;

  private String accessToken;

  /**
   * InternalAuthInterceptor constructor.
   *
   * @param attempts        number of available login attempts
   * @param authServerUrl   URL of the authorization server
   * @param jwtClientId     JWT client ID
   * @param jwtClientSecret JWT client secret
   * @param tokenResource   authorization server obtain token endpoint
   * @param username        internal user username
   * @param password        internal user password
   */
  public InternalAuthInterceptor(int attempts, String authServerUrl, String jwtClientId, String jwtClientSecret,
      String tokenResource, String username, String password) {
    this.attempts = attempts;
    this.authServerUrl = authServerUrl;
    this.jwtClientId = jwtClientId;
    this.jwtClientSecret = jwtClientSecret;
    this.tokenResource = tokenResource;
    this.username = username;
    this.password = password;
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Generate payload that will be used for authorization.
   *
   * @return HttpEntity payload that will be used for authorization
   */
  private HttpEntity<MultiValueMap<String, String>> authPayload() {
    log.debug("Creating authorization payload");
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.add("Authorization", encodeBasicAuth());
    MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
    body.add("username", username);
    body.add("password", password);
    body.add("grant_type", "password");
    HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(body, headers);
    log.debug("Authorization payload created");

    return payload;
  }

  /**
   * Decode encoded access token.
   *
   * @return Map decoded access token as JSON map
   */
  private Map<String, Object> decodeAccessToken() {
    log.debug("Decoding access token [accessToken = {}]", accessToken);
    Map<String, Object> decoded = new HashMap<>();

    if (accessToken != null) {
      try {
        Decoder decoder = Base64.getDecoder();
        String[] chunks = accessToken.split("\\.");
        decoded = objectMapper.readValue(new String(decoder.decode(chunks[1])),
            new TypeReference<Map<String, Object>>() {
            });
      } catch (Exception ex) {
        log.error("Access token decoding failed", ex);
      }
    }

    log.debug("Access token decoded");

    return decoded;
  }

  /**
   * Encode basic authorization string (value ready to paste in request header).
   *
   * @return String encoded basic authorization string (request header value)
   */
  private String encodeBasicAuth() {
    log.debug("Encoding basic authorization string [jwtClientId = {}, jwtClientSecret = {}]", jwtClientId,
        jwtClientSecret);
    String auth = jwtClientId + ":" + jwtClientSecret;
    String encoded = Base64.getEncoder().encodeToString(auth.getBytes());
    String headerValue = "Basic " + encoded;
    log.debug("Basic authorization string encoded [headerValue = {}]", headerValue);

    return headerValue;
  }

  /**
   * Check if access token is valid (expiration date).
   *
   * @return boolean logical value if access token is valid
   */
  private boolean isAccessTokenValid() {
    log.debug("Checking if access token is valid [accessToken = {}]", accessToken);
    boolean valid = false;

    if (accessToken != null) {
      Integer exp = (Integer) decodeAccessToken().get("exp");
      OffsetDateTime expDate = OffsetDateTime.ofInstant(Instant.ofEpochSecond(Long.valueOf(exp)),
          ZoneId.systemDefault());
      log.debug("Access token expire date [expDate = {}]", expDate);

      if (OffsetDateTime.now().isBefore(expDate)) {
        valid = true;
      }
    }

    log.debug("Access token checked [valid = {}]", valid);

    return valid;
  }

  /**
   * Obtain access token for internal system user.
   */
  private void obtainAccessToken() {
    log.debug("Obtaining internal user access token from authorization server [accessToken = {}]", accessToken);

    if (!isAccessTokenValid()) {
      RestTemplate restTemplate = new RestTemplate();
      String url = authServerUrl + tokenResource;
      ResponseEntity<Map<String, Object>> response = null;
      int attempt = 0;

      while (attempt < attempts) {
        log.debug("Login attempt [attempt = {}]", attempt + 1);

        try {
          response = restTemplate.exchange(url, HttpMethod.POST, authPayload(),
              new ParameterizedTypeReference<Map<String, Object>>() {
              });
          Map<String, Object> body = response.getBody();
          accessToken = (String) body.get("access_token");
          break;
        } catch (HttpClientErrorException hcee) {
          attempt++;
          log.warn("Client error occurred", hcee);
        } catch (HttpServerErrorException hsee) {
          attempt++;
          log.warn("Server error occurred", hsee);
        } catch (Exception ex) {
          attempt++;
          log.warn("Unknown error occurred", ex);
        }
      }
    }

    log.debug("Internal user access token obtained from authorization server [accessToken = {}]", accessToken);
  }

  /**
   * Intercept client site HTTP request with system internal access token
   * (internal authorization).
   *
   * @param request   outgoing request
   * @param body      outgoing request body
   * @param execution outgoing request executor
   * @return ClientHttpResponse response of outgoing request
   * @throws IOException exception thrown when intercepting fails
   */
  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
      throws IOException {
    log.debug("Intercepting internal authorization");
    obtainAccessToken();

    if (accessToken != null) {
      request.getHeaders().add("Authorization", "Bearer " + accessToken);
    }

    request.getHeaders().add("User", JwtUtils.getPrincipalLogin());
    ClientHttpResponse response = execution.execute(request, body);
    log.debug("Internal authorization intercepted");

    return response;
  }

}
