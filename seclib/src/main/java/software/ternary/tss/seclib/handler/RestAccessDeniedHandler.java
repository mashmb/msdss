package software.ternary.tss.seclib.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.web.access.AccessDeniedHandler;

import software.ternary.tss.seclib.exception.OAuth2DetailedException;

/**
 * Access denied error handler for REST API.
 *
 * @author TSS
 */
public class RestAccessDeniedHandler implements AccessDeniedHandler {

  private final ObjectMapper objectMapper;

  /**
   * RestAccessDeniedHandler constructor.
   */
  public RestAccessDeniedHandler() {
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Handle access denied exception (403, no permissions).
   *
   * @param request               HTTP request
   * @param response              HTTP response
   * @param accessDeniedException error details
   * @throws IOException exception thrown when problems with response output
   *                     stream occurs
   */
  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response,
      org.springframework.security.access.AccessDeniedException accessDeniedException) throws IOException {
    Map<String, Object> body = new LinkedHashMap<>();
    body.put(OAuth2DetailedException.MESSAGE_FIELD, "No permission.");
    body.put(OAuth2DetailedException.DETAILS_FIELD, new ArrayList<>());
    response.setStatus(403);
    response.setContentType("application/json");
    objectMapper.writeValue(response.getOutputStream(), body);
  }

}
