package software.ternary.tss.seclib.exception;

import java.util.List;

import lombok.Getter;
import software.ternary.tss.seclib.dto.ApiExceptionDetailsDto;

/**
 * Exception used to communicate REST API errors.
 *
 * @author TSS
 */
public class ApiException extends RuntimeException {

  private static final long serialVersionUID = -158090112089355280L;

  @Getter
  private final int status;

  @Getter
  private final List<ApiExceptionDetailsDto> details;

  /**
   * ApiException constructor.
   *
   * @param status  HTTP status
   * @param message error message
   * @param details error details
   */
  public ApiException(int status, String message, List<ApiExceptionDetailsDto> details) {
    super(message);
    this.status = status;
    this.details = details;
  }

}
