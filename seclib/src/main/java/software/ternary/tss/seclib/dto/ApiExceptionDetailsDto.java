package software.ternary.tss.seclib.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Detailed REST API error.
 *
 * @author TSS
 */
@Data
@NoArgsConstructor
@JsonDeserialize(builder = ApiExceptionDetailsDto.ApiExceptionDetailsDtoBuilder.class)
public class ApiExceptionDetailsDto {

  private String message;
  private String field;

  /**
   * ApiExceptionDetailsDto constructor.
   *
   * @param message detailed error message
   * @param field   field for which detailed message was generated
   */
  @Builder
  public ApiExceptionDetailsDto(String message, String field) {
    this.message = message;
    this.field = field;
  }

  /**
   * Detailed REST API error JSON builder.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class ApiExceptionDetailsDtoBuilder {

  }

}
