package software.ternary.tss.seclib.dto;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * External data structure for REST API error.
 * 
 * @author TSS
 */
@Data
@NoArgsConstructor
@JsonDeserialize(builder = ApiExceptionDto.ApiExceptionDtoBuilder.class)
public class ApiExceptionDto {

  private String message;
  private List<ApiExceptionDetailsDto> details;

  /**
   * ApiExceptionDto constructor.
   *
   * @param message error message
   * @param details error details
   */
  @Builder
  public ApiExceptionDto(String message, List<ApiExceptionDetailsDto> details) {
    this.message = message;
    this.details = details;
  }

  /**
   * REST API error JSON builder.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class ApiExceptionDtoBuilder {

  }

}
