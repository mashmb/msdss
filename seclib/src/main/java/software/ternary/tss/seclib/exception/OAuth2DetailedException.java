package software.ternary.tss.seclib.exception;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

import lombok.Getter;
import software.ternary.tss.seclib.serializer.OAuth2ExceptionSerializer;

/**
 * Exception used to handle authentication errors.
 *
 * @author TSS
 */
@JsonSerialize(using = OAuth2ExceptionSerializer.class)
public class OAuth2DetailedException extends OAuth2Exception {

  private static final long serialVersionUID = 3788312298987093517L;

  public static final String MESSAGE_FIELD = "message";
  public static final String DETAILS_FIELD = "details";

  @Getter
  private final int status;

  @Getter
  private final List<OAuth2ExceptionDetails> details;

  /**
   * OAuth2DetailedException constructor.
   *
   * @param status  HTTP status for error
   * @param message error message
   * @param details error details
   */
  public OAuth2DetailedException(int status, String message, List<OAuth2ExceptionDetails> details) {
    super(message);
    this.status = status;
    this.details = details;
  }

  /**
   * Details for error connected with authentication.
   */
  public static class OAuth2ExceptionDetails {

    public static final String MESSAGE_FIELD = "message";
    public static final String FIELD_FIELD = "field";

    @Getter
    private final String message;

    @Getter
    private final String field;

    /**
     * OAuth2ExceptionDetails constructor.
     *
     * @param message error message
     * @param field   name of the field that generated error
     */
    public OAuth2ExceptionDetails(String message, String field) {
      this.message = message;
      this.field = field;
    }

  }

}
