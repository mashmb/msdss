package software.ternary.tss.seclib.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.extern.slf4j.Slf4j;

/**
 * Utils connected with HTTP request.
 *
 * @author TSS
 */
@Slf4j
public class HttpRequestUtils {

  /**
   * Get current HTTP request for later processing.
   *
   * @return HttpServletRequest current HTTP request
   */
  public static HttpServletRequest getCurrentRequest() {
    log.debug("Getting current request");
    RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = ((ServletRequestAttributes) attributes).getRequest();
    log.debug("Current request got [request = {}]", request);

    return request;
  }

}
