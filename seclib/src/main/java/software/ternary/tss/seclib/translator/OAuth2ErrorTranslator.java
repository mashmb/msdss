package software.ternary.tss.seclib.translator;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.exception.OAuth2DetailedException;

/**
 * Translator for errors connected strictly with OAuth 2.
 *
 * @author TSS
 */
@Slf4j
public class OAuth2ErrorTranslator implements WebResponseExceptionTranslator<OAuth2Exception> {

  /**
   * Translate error to HTTP response.
   *
   * @param e error that occurred
   * @throws Exception exception thrown when error is forwarded
   */
  @Override
  public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
    if (e instanceof OAuth2DetailedException) {
      return ResponseEntity.status(((OAuth2DetailedException) e).getStatus()).body((OAuth2DetailedException) e);
    } else {
      log.error("Token error occurred", e);
      return ResponseEntity.status(401).body(new OAuth2DetailedException(401, "Not authorized", null));
    }
  }

}
