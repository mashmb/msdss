package software.ternary.tss.seclib.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import lombok.extern.slf4j.Slf4j;

/**
 * Utils connected with JWT tokens.
 *
 * @author TSS
 */
@Slf4j
public class JwtUtils {

  /**
   * Extract login of authenticated user from JWT token.
   *
   * @return String login of authenticated user from JWT token
   */
  public static String getLogin() {
    log.debug("Extracting authenticated user login from JWT token");
    String login = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null
        && auth.getAuthorities().stream().noneMatch(a -> a.getAuthority().equalsIgnoreCase("ROLE_ANONYMOUS"))) {
      login = auth.getName();
    }

    log.debug("Authenticated user login extracted from JWT token [login = {}]", login);

    return login;
  }

  /**
   * Extract login of principal user from JWT token. In internal system
   * communication, token contains internal user data. External user login
   * (principal) can be identified with header value.
   *
   * @return String login of principal user from JWT token
   */
  public static String getPrincipalLogin() {
    log.debug("Extracting principal user login from JWT token");
    String principal = HttpRequestUtils.getCurrentRequest().getHeader("User");

    if (principal == null) {
      principal = getLogin();
    }

    log.debug("Principal user login extracted from JWT token [principal = {}]", principal);

    return principal;
  }

  /**
   * Check if authenticated user has permission (from JWT token).
   *
   * @param permission name of the permission to check
   * @return boolean logical value if user has permission
   */
  public static boolean hasPermission(String permission) {
    log.debug("Checking if authenticated user has permission [permission = {}]", permission);
    boolean hasPermission = false;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      hasPermission = auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equalsIgnoreCase(permission));
    }

    log.debug("Authenticated user checked for permission [hasPermission = {}]", hasPermission);

    return hasPermission;
  }

}
