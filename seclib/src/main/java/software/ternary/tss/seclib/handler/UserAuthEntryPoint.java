package software.ternary.tss.seclib.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import software.ternary.tss.seclib.exception.OAuth2DetailedException;

/**
 * Configuration for user authentication entry point.
 *
 * @author TSS
 */
public class UserAuthEntryPoint implements AuthenticationEntryPoint {

  private final ObjectMapper objectMapper;

  /**
   * UserAuthEntryPoint constructor.
   */
  public UserAuthEntryPoint() {
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Commence authentication scheme.
   *
   * @param request       HTTP request
   * @param response      HTTP response
   * @param authException authentication error details
   * @throws IOException exception thrown when problems with response output
   *                     stream occurs
   */
  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
      throws IOException {
    Map<String, Object> body = new LinkedHashMap<>();
    body.put(OAuth2DetailedException.MESSAGE_FIELD, "Not authorized.");
    body.put(OAuth2DetailedException.DETAILS_FIELD, new ArrayList<>());
    response.setStatus(401);
    response.setContentType("application/json");
    objectMapper.writeValue(response.getOutputStream(), body);
  }

}
