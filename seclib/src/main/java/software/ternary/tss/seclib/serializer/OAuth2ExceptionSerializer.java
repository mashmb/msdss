package software.ternary.tss.seclib.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import software.ternary.tss.seclib.exception.OAuth2DetailedException;

/**
 * Serializer used to handle OAuth 2 exceptions.
 *
 * @author TSS
 */
public class OAuth2ExceptionSerializer extends StdSerializer<OAuth2DetailedException> {

  private static final long serialVersionUID = 3856832143233867544L;

  /**
   * OAuth2ExceptionSerializer constructor.
   */
  protected OAuth2ExceptionSerializer() {
    super(OAuth2DetailedException.class);
  }

  /**
   * Serialize OAuth 2 error to JSON.
   *
   * @param value    OAuth 2 error
   * @param gen      JSON generator
   * @param provider serializer
   * @throws IOException exception thrown when serialization failed
   */
  @Override
  public void serialize(OAuth2DetailedException value, JsonGenerator gen, SerializerProvider provider)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField(OAuth2DetailedException.MESSAGE_FIELD, value.getMessage());
    gen.writeFieldName(OAuth2DetailedException.DETAILS_FIELD);
    gen.writeStartArray();

    if (value.getDetails() != null) {
      for (OAuth2DetailedException.OAuth2ExceptionDetails detail : value.getDetails()) {
        gen.writeStartObject();
        gen.writeStringField(OAuth2DetailedException.OAuth2ExceptionDetails.FIELD_FIELD, detail.getField());
        gen.writeStringField(OAuth2DetailedException.OAuth2ExceptionDetails.MESSAGE_FIELD, detail.getMessage());
        gen.writeEndObject();
      }
    }

    gen.writeEndArray();
    gen.writeEndObject();
  }

}
