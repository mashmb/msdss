package software.ternary.tss.seclib.handler;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import software.ternary.tss.seclib.dto.ApiExceptionDto;
import software.ternary.tss.seclib.exception.ApiException;

/**
 * Handler for REST API errors.
 *
 * @author TSS
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestApiErrorHandler extends ResponseEntityExceptionHandler {

  /**
   * Handle REST API errors connected with business directly.
   *
   * @param exception internal application exception
   * @return ResponseEntity internal application exception translated for outside
   *         World
   */
  @ExceptionHandler(ApiException.class)
  protected ResponseEntity<ApiExceptionDto> handleApiException(ApiException exception) {
    ApiExceptionDto dto = ApiExceptionDto.builder().message(exception.getMessage()).details(exception.getDetails())
        .build();

    return ResponseEntity.status(exception.getStatus()).body(dto);
  }

}
