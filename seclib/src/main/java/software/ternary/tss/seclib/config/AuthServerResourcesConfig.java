package software.ternary.tss.seclib.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.handler.RestAccessDeniedHandler;
import software.ternary.tss.seclib.handler.UserAuthEntryPoint;

/**
 * Resources configuration.
 *
 * @author TSS
 */
@Slf4j
@Configuration
@EnableResourceServer
@Profile("auth-server")
class AuthServerResourcesConfig extends ResourceServerConfigurerAdapter {

  @Value("${security.public-resources}")
  private String[] publicResources;

  /**
   * Configure secured resources.
   *
   * @param http current HTTP filter configuration
   * @throws Exception exception thrown when configuration failed
   */
  @Override
  public void configure(HttpSecurity http) throws Exception {
    log.debug("Configuring secured resources [publicResources = {}, http = {}]", publicResources, http);
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().exceptionHandling()
        .accessDeniedHandler(new RestAccessDeniedHandler()).and().cors().and().csrf().disable().authorizeRequests()
        .antMatchers(publicResources).permitAll().anyRequest().authenticated();
    log.debug("Secured resources configured [publicResources = {}, http = {}]", publicResources, http);
  }

  /**
   * Configure resource server properties.
   *
   * @param resources configurer for resource server
   */
  @Override
  public void configure(ResourceServerSecurityConfigurer resources) {
    log.debug("Configuring resource server properties [resources = {}]", resources);
    resources.authenticationEntryPoint(new UserAuthEntryPoint());
    log.debug("Resource server properties configured [resources = {}]", resources);
  }

}
