package software.ternary.tss.seclib.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.exception.OAuth2DetailedException;
import software.ternary.tss.seclib.handler.RestAccessDeniedHandler;
import software.ternary.tss.seclib.handler.UserAuthEntryPoint;

/**
 * Resources configuration.
 *
 * @author TSS
 */
@Slf4j
@Configuration
@EnableResourceServer
@Profile("auth-server-client")
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
class AuthClientResourcesConfig extends ResourceServerConfigurerAdapter {

  @Value("${security.jwt.client.id}")
  private String jwtClientId;

  @Value("${security.jwt.client.secret}")
  private String jwtClientSecret;

  @Value("${security.authorization-server}")
  private String authServerUrl;

  @Value("${security.jwt.token.endpoint.check-token:/oauth/check_token}")
  private String checkTokenResource;

  @Value("${security.public-resources}")
  private String[] publicResources;

  /**
   * Configure remote token store (external authorization server).
   *
   * @return ResourceServerTokenServices configured remote token store
   */
  @Bean
  public ResourceServerTokenServices remoteTokenService() {
    log.debug("Configuring remote token store service");
    RemoteTokenServices service = new RemoteTokenServices();
    service.setClientId(jwtClientId);
    service.setClientSecret(jwtClientSecret);
    service.setCheckTokenEndpointUrl(authServerUrl + checkTokenResource);
    RestTemplate restTemplate = new RestTemplate();

    restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

      @Override
      public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getRawStatusCode() != 200) {
          throw new OAuth2DetailedException(401, "Not authorized", null);
        }
      }

    });

    service.setRestTemplate(restTemplate);
    log.debug("Remote token store service configured [service = {}]", service);

    return service;
  }

  /**
   * Configure secured resources.
   *
   * @param http current HTTP filter configuration
   * @throws Exception exception thrown when configuration failed
   */
  @Override
  public void configure(HttpSecurity http) throws Exception {
    log.debug("Configuring secured resources [publicResources = {}, http = {}]", publicResources, http);
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().exceptionHandling()
        .accessDeniedHandler(new RestAccessDeniedHandler()).and().cors().and().csrf().disable().authorizeRequests()
        .antMatchers(publicResources).permitAll().anyRequest().authenticated();
    log.debug("Secured resources configured [publicResources = {}, http = {}]", publicResources, http);
  }

  /**
   * Configure resource server properties.
   *
   * @param resources configurer for resource server
   */
  @Override
  public void configure(ResourceServerSecurityConfigurer resources) {
    log.debug("Configuring resource server properties [resources = {}]", resources);
    resources.authenticationEntryPoint(new UserAuthEntryPoint());
    log.debug("Resource server properties configured [resources = {}]", resources);
  }

}
