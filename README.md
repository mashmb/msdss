# MSDSS - Microservices Distributed Security

Security layer for distributed microservices environment. The main aim of the project was to implement OAuth 2 standard 
not only for communication between user and system but also between microservices that are building the system (internal 
user in internal communication). Secured communication is provided by JWT tokens. In internal communication username of 
external user (system client) is transferred in HTTP request headers, so it can be identified in every microservice during 
internal requests chain. Solution is production ready, it will work properly without/with PROXY at the edge of the system 
or with usage of routing gateway. 

Project is written in Java 8 with usage of Spring Boot projects. Authorization was also realized with Spring Boot 
(older versions of dependencies was used because Spring Boot officially no longer supports authorization servers - it 
was to hard to maintain, there is a lot of other great open source solutions to achieve the aim like Keycloak).

## Components

MSDSS project contains 4 components:

- **authserver** - authorization server implementation; it can work in memory or with database usage (database implementation 
was created for multi instance work); user registration; user log in; user log out; 3 REST API endpoints to check how 
it works (public, authorization required, permission required)
- **ms1** - example microservice used to check integration with authorization server; 3 REST API endpoints to check how 
it works (public, authorization required, permission required)
- **ms2** - example microservice used to check integration with authorization server and ms1; one REST API endpoint used 
to check if external user will be recognized in communication by internal user with ms1
- **seclib** - some kind of stub (security library); every configured microservice with usage of library can be authorization 
server or authorization server client; REST API error handlers; security layer utils (get logged in user username, check 
if user has permission, etc.)

## External authorization

Below diagram shows how it works when external user wants to interact with system.

![external](./docs/external.png)

1. External user uses **Basic Auth** to obtain JWT token from authorization server. If user gave proper login and password, 
authorization server returns JWT access token.
2. External user sends HTTP request to ms1 with JWT access token in headers and waits for HTTP response from ms1.
3. Ms1 can not validate JWT access token independently so external user JWT access token is forwarded to authorization 
server for validation. Authorization server returns information about token validity. If everything is okay, ms1 returns 
HTTP response to external user.

## Internal authorization

Below diagram shows how it works when external user wants to interact with system and system components needs to communicate 
with each other.

![internal](./docs/internal.png)

1. External user uses **Basic Auth** to obtain JWT token from authorization server. If user gave proper login and password, 
authorization server returns JWT access token.
2. External user sends HTTP request to ms2 with JWT access token in headers and waits for HTTP response from ms2.
3. Ms2 can not validate JWT access token independently so external user JWT access token is forwarded to authorization 
server for validation. Authorization server returns information about token validity. If everything is okay, ms2 continues 
request processing.
4. Ms2 is logging in as internal user with usage of **Basic Auth**. There is JWT access token for internal system user 
in HTTP response from authorization server.
5. Ms2 uses internal user JWT access token and sends HTTP request to ms1 (external user username is placed in HTTP request 
headers).
6. Ms1 can not validate JWT access token independently so internal system user JWT access token is forwarded to authorization 
server for validation.

## How to run the project?

Project is fully Dockerized. The only one requirement on your machine is to have Docker installed. Firstly run **docker-build.sh** 
script. The next step is to use **docker-compose.yml** file. The last step is to clean your machine, so you can run 
**docker-clean.sh** script.

### Tests

Project was tested with usage of jMeter 5 (**jmeter** directory). Tests was created in 3 parts:

1. **Authserver** - tests of authorization server only
2. **MS1** - tests of ms1 integration with authorization server
3. **MS3** - tests of ms2 integration with ms1 and authorization server
