#!/bin/bash
# 
# Application development build script.
# 
# @author TSS

./gradlew clean
./gradlew build
java -jar build/libs/ms1-1.0.0.jar --server.port=8080 --spring.profiles.active=auth-server-client
