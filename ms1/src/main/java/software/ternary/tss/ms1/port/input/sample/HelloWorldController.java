package software.ternary.tss.ms1.port.input.sample;

import org.springframework.http.ResponseEntity;

import software.ternary.tss.ms1.core.domain.dto.HelloMessageDto;

/**
 * Definition of hello world controller.
 *
 * @author TSS
 */
public interface HelloWorldController {

  /**
   * Message for authorized user.
   *
   * @return ResponseEntity HTTP response with message
   */
  ResponseEntity<HelloMessageDto> getPrivateMessage();

  /**
   * Message for authorized user with permission.
   *
   * @return ResponseEntity HTTP response with message
   */
  ResponseEntity<HelloMessageDto> getPrivateSecuredMessage();

  /**
   * Message for any user (guests also).
   *
   * @return ResponseEntity HTTP response with message
   */
  ResponseEntity<HelloMessageDto> getPublicMessage();

}
