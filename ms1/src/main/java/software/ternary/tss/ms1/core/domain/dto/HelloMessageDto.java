package software.ternary.tss.ms1.core.domain.dto;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Hello message data structure.
 *
 * @author TSS
 */
@Data
@NoArgsConstructor
@JsonDeserialize(builder = HelloMessageDto.HelloMessageDtoBuilder.class)
public class HelloMessageDto {

  private String timestamp;
  private String message;

  /**
   * HelloMessageDto constructor.
   *
   * @param timestamp message timestamp
   * @param message   real message
   */
  @Builder
  public HelloMessageDto(OffsetDateTime timestamp, String message) {
    this.timestamp = timestamp.withNano(0).toString();
    this.message = message;
  }

  /**
   * Hello message JSON builder.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class HelloMessageDtoBuilder {

  }

}
