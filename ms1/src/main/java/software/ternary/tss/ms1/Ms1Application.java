package software.ternary.tss.ms1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * First example microservice main entry point.
 *
 * @author TSS
 */
@SpringBootApplication
public class Ms1Application {

  /**
   * Application main entry point.
   *
   * @param args application arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(Ms1Application.class, args);
  }

}
