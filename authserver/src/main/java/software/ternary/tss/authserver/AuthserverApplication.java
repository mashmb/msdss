package software.ternary.tss.authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Authorization server main entry point.
 * 
 * @author TSS
 */
@SpringBootApplication
public class AuthserverApplication {

  /**
   * Application main entry point.
   *
   * @param args application arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(AuthserverApplication.class, args);
  }

}
