package software.ternary.tss.authserver.port.input.admin;

import org.springframework.http.ResponseEntity;

import software.ternary.tss.authserver.core.domain.dto.UserDto;

/**
 * Definition of user controller.
 *
 * @author TSS
 */
public interface UserController {

  /**
   * Logout actually authenticated user.
   *
   * @return ResponseEntity HTTP response with operation status
   */
  ResponseEntity<Void> logout();

  /**
   * Register new user in application.
   *
   * @param userData new user data
   * @return ResponseEntity HTTP response for registration
   */
  ResponseEntity<Void> registerUser(UserDto userData);

}
