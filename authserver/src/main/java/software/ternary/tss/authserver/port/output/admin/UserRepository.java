package software.ternary.tss.authserver.port.output.admin;

import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Definition of user repository.
 *
 * @author TSS
 */
public interface UserRepository {

  /**
   * Find user by email address.
   *
   * @param email email address
   * @return User user found for given email address
   */
  User findFirtByEmail(String email);

  /**
   * Save user data.
   * 
   * @param user user data to save
   * @return User user data after save
   */
  User save(User user);

}
