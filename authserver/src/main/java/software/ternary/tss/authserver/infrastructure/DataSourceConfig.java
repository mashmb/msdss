package software.ternary.tss.authserver.infrastructure;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import lombok.extern.slf4j.Slf4j;

/**
 * Application data source configuration.
 * 
 * @author TSS
 */
@Slf4j
@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@EnableJpaRepositories(considerNestedRepositories = true, basePackages = {
    "software.ternary.tss.authserver.adapter.output" })
class DataSourceConfig {

  @Value("${data-source.driver:}")
  private String driver;

  @Value("${data-source.url:}")
  private String url;

  @Value("${data-source.username:}")
  private String username;

  @Value("${data-source.password:}")
  private String password;

  /**
   * Configure application external data source.
   *
   * @return DataSource configured access to external data source.
   */
  @Bean
  @Profile("db")
  public DataSource dataSource() {
    log.debug("Configuring application data source");
    DataSourceBuilder<?> builder = DataSourceBuilder.create();
    builder.driverClassName(driver);
    builder.url(url);
    builder.username(username);
    builder.password(password);
    log.debug("Application data source configured [builder = {}]", builder);

    return builder.build();
  }

  /**
   * Populate application external data source with initial state.
   * 
   * @param dataSource access to configured external application data source
   * @return DataSourceInitializer initializer that will be used to populate
   *         initial state
   */
  @Bean
  @Profile("db")
  public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
    log.debug("Initializing external data source initial state");
    ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
    populator.addScript(new ClassPathResource("/init-db.sql"));
    DataSourceInitializer initializer = new DataSourceInitializer();
    initializer.setDataSource(dataSource);
    initializer.setDatabasePopulator(populator);
    log.debug("External data source initial state initialized");

    return initializer;
  }

}
