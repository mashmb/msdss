package software.ternary.tss.authserver.port.output.security;

import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Definition of security output actions.
 *
 * @author TSS
 */
public interface SecurityPort {

  /**
   * Logout user.
   *
   * @param user user to logout
   */
  void logoutUser(User user);

}
