package software.ternary.tss.authserver.infrastructure.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;

/**
 * External user representation (in JWT token).
 * 
 * @author TSS
 */
class JwtUser extends User {

  private static final long serialVersionUID = 1108579057426109864L;

  @Getter
  private final String firstName;

  @Getter
  private final String lastName;

  /**
   * JwtUser constructor.
   *
   * @param username    user username
   * @param password    user password
   * @param authorities user authorities
   * @param firstName   user first name
   * @param lastName    user last name
   */
  public JwtUser(String username, String password, Collection<? extends GrantedAuthority> authorities, String firstName,
      String lastName) {
    super(username, password, true, true, true, true, authorities);
    this.firstName = firstName;
    this.lastName = lastName;
  }

}
