package software.ternary.tss.authserver.infrastructure.security;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Implementation of security events publisher.
 *
 * @author TSS
 */
@Slf4j
@Component
class SecurityEventPublisher implements EventPublisher {

  private ApplicationEventPublisher eventPublisher;

  /**
   * SecurityEventPublisher constructor.
   *
   * @param eventPublisher access to application event publisher
   */
  public SecurityEventPublisher(ApplicationEventPublisher eventPublisher) {
    this.eventPublisher = eventPublisher;
  }

  /**
   * Publish user logout event.
   *
   * @param user user to logout
   */
  @Override
  public void publishLogoutEvent(User user) {
    log.debug("Publishing logout event for user [username = {}]", user.getEmail());
    LogoutEvent event = new LogoutEvent(this, user);
    eventPublisher.publishEvent(event);
    log.debug("Logout event for user published [username = {}]", user.getEmail());
  }

}
