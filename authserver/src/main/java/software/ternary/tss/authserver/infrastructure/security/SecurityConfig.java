package software.ternary.tss.authserver.infrastructure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.extern.slf4j.Slf4j;

/**
 * Security configuration.
 * 
 * @author TSS
 */
@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final AuthenticationProvider authProvider;
  private final UserDetailsService userService;

  /**
   * SecurityConfig constructor.
   *
   * @param authProvider access to authentication provider
   * @param userService  access to user service in security layer
   */
  public SecurityConfig(AuthenticationProvider authProvider, UserDetailsService userService) {
    this.authProvider = authProvider;
    this.userService = userService;
  }

  /**
   * Initialize password encoder.
   *
   * @return PasswordEncoder initialized password encoder
   */
  @Bean
  public PasswordEncoder passwordEncoder() {
    log.debug("Initializing password encoder");
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    log.debug("Password encoder initialized [encoder = {}]", encoder);

    return encoder;
  }

  /**
   * Configure authentication manager.
   *
   * @param auth authentication manager builder to use
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    log.debug("Configuring authentication manager [auth = {}]", auth);
    auth.authenticationProvider(authProvider).userDetailsService(userService).passwordEncoder(passwordEncoder());
    log.debug("Authentication manager configured [auth = {}]", auth);
  }

  /**
   * Initialize pre configured authentication manager.
   *
   * @return AuthenticationManager initialized pre configured authentication
   *         manager
   * @throws Exception exception thrown when initialization or configuration fails
   */
  @Bean
  @Override
  protected AuthenticationManager authenticationManager() throws Exception {
    log.debug("Initializing authentication manager");
    AuthenticationManager manager = super.authenticationManager();
    log.debug("Authentication manager initialized [manager = {}]", manager);

    return manager;
  }

}
