package software.ternary.tss.authserver.infrastructure.security;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import lombok.extern.slf4j.Slf4j;

/**
 * JWT access token converter.
 *
 * @author TSS
 */
@Slf4j
class AccessTokenConverter extends JwtAccessTokenConverter {

  /**
   * Enhance JWT access token with additional (optional) data.
   *
   * @param accessToken    current access token (raw, without additional info)
   * @param authentication current authentication
   * @return OAuth2AccessToken JWT access token with additional data
   */
  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
    log.debug("Enhancing JWT access token");
    JwtUser user = (JwtUser) authentication.getPrincipal();
    Map<String, Object> extra = new LinkedHashMap<>();
    extra.put("first_name", user.getFirstName());
    extra.put("last_name", user.getLastName());
    ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(extra);
    accessToken = super.enhance(accessToken, authentication);
    ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(new HashMap<>());
    log.debug("JWT access token enhanced");

    return accessToken;
  }

}
