package software.ternary.tss.authserver.infrastructure.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.seclib.translator.OAuth2ErrorTranslator;

/**
 * Authorization server configuration.
 *
 * @author TSS
 */
@Slf4j
@Configuration
@EnableAuthorizationServer
class AuthConfig extends AuthorizationServerConfigurerAdapter implements ApplicationListener<LogoutEvent> {

  private final AuthenticationManager authManager;
  private final DataSource dataSource;
  private final PasswordEncoder passwordEncoder;
  private final UserDetailsService userService;

  @Value("${security.jwt.client.id}")
  private String jwtClientId;

  @Value("${security.jwt.client.secret}")
  private String jwtClientSecret;

  @Value("${security.jwt.grant-types}")
  private String[] jwtGrantTypes;

  @Value("${security.jwt.scopes}")
  private String[] jwtScopes;

  @Value("${security.jwt.token.access-validity}")
  private int jwtAccessTokenValidity;

  @Value("${security.jwt.token.refresh-validity}")
  private int jwtRefreshTokenValidity;

  @Value("${security.jwt.token.endpoint.token:/oauth/token}")
  private String tokenResource;

  @Value("${security.jwt.token.endpoint.check-token:/oauth/check_token}")
  private String checkTokenResource;

  /**
   * AuthConfig constructor.
   *
   * @param authManager     access to authentication manager
   * @param dataSource      access to optional external application data source
   * @param passwordEncoder access to password encoder
   * @param userService     access to user service
   */
  public AuthConfig(AuthenticationManager authManager, Optional<DataSource> dataSource, PasswordEncoder passwordEncoder,
      UserDetailsService userService) {
    this.authManager = authManager;
    this.dataSource = dataSource.orElse(null);
    this.passwordEncoder = passwordEncoder;
    this.userService = userService;
  }

  /**
   * Initialize JWT access token converter.
   *
   * @return JwtAccessTokenConverter initialized access token converter
   */
  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    log.debug("Initializing JWT access token converter");
    JwtAccessTokenConverter converter = new AccessTokenConverter();
    log.debug("JWT access token converted initialized [converter = {}]", converter);

    return converter;
  }

  /**
   * Initialize token services.
   *
   * @return DefaultTokenServices initialized token services
   */
  @Bean
  public DefaultTokenServices tokenServices() {
    log.debug("Initializing token services");
    TokenEnhancerChain chain = new TokenEnhancerChain();
    chain.setTokenEnhancers(Arrays.asList(accessTokenConverter()));
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<>(userService));
    DefaultTokenServices tokenServices = new SyncTokenServices();
    tokenServices.setTokenStore(tokenStore());
    tokenServices.setAuthenticationManager(new ProviderManager(Arrays.asList(provider)));
    tokenServices.setTokenEnhancer(chain);
    tokenServices.setAccessTokenValiditySeconds(jwtAccessTokenValidity);
    tokenServices.setRefreshTokenValiditySeconds(jwtRefreshTokenValidity);
    tokenServices.setSupportRefreshToken(true);
    tokenServices.setReuseRefreshToken(false);
    log.debug("Token services initialized [services = {}]", tokenServices);

    return tokenServices;
  }

  /**
   * Initialize token store.
   *
   * @return TokenStore initialized token store
   */
  @Bean
  public TokenStore tokenStore() {
    log.debug("Initializing token store");
    TokenStore store;

    if (dataSource != null) {
      store = new JdbcTokenStore(dataSource);
    } else {
      store = new InMemoryTokenStore();
    }

    log.debug("Token store initialized [store = {}]", store);

    return store;
  }

  /**
   * Configure non-security features of the authorization server.
   *
   * @param endpoints the endpoints configurer
   */
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
    log.debug("Configuring non-security features of the authorization server [endpoints = {}]", endpoints);
    endpoints.tokenStore(tokenStore()).reuseRefreshTokens(false).accessTokenConverter(accessTokenConverter())
        .authenticationManager(authManager).userDetailsService(userService).tokenServices(tokenServices())
        .exceptionTranslator(new OAuth2ErrorTranslator()).pathMapping("/oauth/token", tokenResource)
        .pathMapping("/oauth/check_token", checkTokenResource);
    log.debug("Non-security features of the authorization server configured [endpoints = {}]", endpoints);
  }

  /**
   * Configure security of the authorization server.
   *
   * @param security a fluent configurer for security features
   */
  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) {
    log.debug("Configuring security of the authorization server [security = {}]", security);
    security.checkTokenAccess("permitAll()").allowFormAuthenticationForClients();
    log.debug("Security of the authorization server configured [security = {}]", security);
  }

  /**
   * Configure individual clients properties.
   *
   * @param clients the client details configurer
   * @throws Exception exception thrown when configuring ends without success
   */
  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    log.debug("Configuring individual clients properties [clients = {}]", clients);
    clients.inMemory().withClient(jwtClientId).secret(passwordEncoder.encode(jwtClientSecret))
        .accessTokenValiditySeconds(jwtAccessTokenValidity).refreshTokenValiditySeconds(jwtRefreshTokenValidity)
        .authorizedGrantTypes(jwtGrantTypes).scopes(jwtScopes);
    log.debug("Individual clients properties configured [clients = {}]", clients);
  }

  /**
   * Handle emitted user logout event (delete generated access tokens).
   *
   * @param event emitted user logout event
   */
  @Override
  public void onApplicationEvent(LogoutEvent event) {
    log.debug("Handling user logout event [username = {}]", event.getUser().getEmail());
    TokenStore tokenStore = tokenStore();
    Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientIdAndUserName(jwtClientId,
        event.getUser().getEmail());
    tokens.forEach(token -> tokenStore.removeAccessToken(token));
    log.debug("User logout event handled [username = {}]", event.getUser().getEmail());
  }

}
