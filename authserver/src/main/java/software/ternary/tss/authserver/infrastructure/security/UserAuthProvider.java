package software.ternary.tss.authserver.infrastructure.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.seclib.exception.OAuth2DetailedException;

/**
 * OAuth 2 authentication provider.
 *
 * @author TSS
 */
@Slf4j
@Component
class UserAuthProvider implements AuthenticationProvider {

  private final UserDetailsService userService;

  /**
   * UserAuthProvider constructor.
   *
   * @param userService access to user service on security layer
   */
  public UserAuthProvider(UserDetailsService userService) {
    this.userService = userService;
  }

  /**
   * Authenticate user with authentication process.
   *
   * @param authentication authentication request
   * @return Authentication fully authenticated object
   */
  @Override
  public Authentication authenticate(Authentication authentication) {
    log.debug("Authenticating user");
    String password = authentication.getCredentials() != null ? authentication.getCredentials().toString() : "";
    User systemUser = ((SecurityUserService) userService).loadSystemUserByUsername(authentication.getName());

    if (systemUser != null) {
      if (!BCrypt.checkpw(password, systemUser.getPassword())) {
        log.warn("Invalid password");
        throw new OAuth2DetailedException(400, "Invalid password", null);
      }
    } else {
      log.warn("Invalid username");
      throw new OAuth2DetailedException(400, "Invalid username", null);
    }

    JwtUser externalUser = (JwtUser) userService.loadUserByUsername(authentication.getName());
    log.debug("User authenticated");

    return new UsernamePasswordAuthenticationToken(externalUser, null, externalUser.getAuthorities());
  }

  /**
   * Check if authentication object is supported.
   *
   * @param authentication authentication object
   * @return boolean logical value if authentication object is supported
   */
  @Override
  public boolean supports(Class<?> authentication) {
    log.debug("Checking if authentication object is supported");
    boolean supported = authentication.equals(UsernamePasswordAuthenticationToken.class);
    log.debug("Checked if authentication object is supported [supported = {}]", supported);

    return supported;
  }

}
