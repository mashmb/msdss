package software.ternary.tss.authserver.adapter.output.admin;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.port.output.admin.UserRepository;

/**
 * Implementation of user repository in memory.
 *
 * @author TSS
 */
@Slf4j
@Repository
@Profile("in-memory")
class InMemoryUserRepository implements UserRepository {

  private final Set<User> extent = new HashSet<>();

  /**
   * Find user by email address.
   *
   * @param email email address
   * @return User user found for given email address
   */
  @Override
  public User findFirtByEmail(String email) {
    log.debug("Start of UserRepository.findFirtByEmail [email = {}]", email);
    User user = extent.stream().filter(u -> u.getEmail().equalsIgnoreCase(email)).findFirst().orElse(null);
    log.debug("End of UserRepository.findFirtByEmail [usrId = {}]", user != null ? user.getUsrId() : null);

    return user;
  }

  /**
   * Save user data.
   * 
   * @param user user data to save
   * @return User user data after save
   */
  @Override
  public User save(User user) {
    log.debug("Start of UserRepository.save [usrId = {}]", user.getUsrId());

    if (user.getUsrId() == null) {
      int lastId = extent.stream().mapToInt(u -> u.getUsrId()).max().orElse(0);
      user.setUsrId(lastId + 1);
      extent.add(user);
    } else {
      User prevData = extent.stream().filter(u -> u.getUsrId() == user.getUsrId()).findFirst().get();
      extent.remove(prevData);
      extent.add(user);
    }

    log.debug("End of UserRepository.save [usrId = {}]", user.getUsrId());

    return user;
  }

}
