package software.ternary.tss.authserver.infrastructure.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

/**
 * Synchronized token services.
 *
 * @author TSS
 */
class SyncTokenServices extends DefaultTokenServices {

  /**
   * Create an access token associated with specified credentials in synchronous
   * way.
   *
   * @param authentication the credentials associated with the access token
   * @return OAuth2AccessToken the access token
   * @throws AuthenticationException exception thrown when credentials are
   *                                 inadequate
   */
  @Override
  public synchronized OAuth2AccessToken createAccessToken(OAuth2Authentication authentication)
      throws AuthenticationException {
    return super.createAccessToken(authentication);
  }

  /**
   * Refresh the access token in synchronized way.
   * 
   * @param refreshTokenValue the details about refresh token
   * @param tokenRequest      the incoming token request
   * @return OAuth2AccessToken the (new) access token
   * @throws AuthenticationException exception thrown when the refresh token is
   *                                 invalid or expired
   */
  @Override
  public synchronized OAuth2AccessToken refreshAccessToken(String refreshTokenValue, TokenRequest tokenRequest)
      throws AuthenticationException {
    return super.refreshAccessToken(refreshTokenValue, tokenRequest);
  }

}
