package software.ternary.tss.authserver.infrastructure.security;

import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Definition of application event publisher.
 *
 * @author TSS
 */
public interface EventPublisher {

  /**
   * Publish user logout event.
   *
   * @param user user to logout
   */
  void publishLogoutEvent(User user);

}
