package software.ternary.tss.authserver.adapter.output.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.port.output.util.Encoder;

/**
 * Implementation of encoding features.
 *
 * @author TSS
 */
@Slf4j
@Component
class UserPasswordEncoder implements Encoder {

  private final PasswordEncoder passwordEncoder;

  /**
   * UserPasswordEncoder constructor.
   *
   * @param passwordEncoder access to password encoder
   */
  public UserPasswordEncoder() {
    this.passwordEncoder = new BCryptPasswordEncoder();
  }

  /**
   * Encode given text by encoder.
   *
   * @param toEncode text to encode by encoder
   * @return String encoded by encoder text
   */
  @Override
  public String encode(String toEncode) {
    log.debug("Encoding");
    String encoded = passwordEncoder.encode(toEncode);
    log.debug("Encoded");

    return encoded;
  }

}
