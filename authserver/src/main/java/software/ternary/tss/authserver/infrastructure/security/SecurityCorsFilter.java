package software.ternary.tss.authserver.infrastructure.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * CORS configuration for security layer.
 *
 * @author TSS
 */
@Configuration
@WebFilter("/*")
@Order(Ordered.HIGHEST_PRECEDENCE)
class SecurityCorsFilter implements Filter {

  /**
   * Disable default CORS policy for security layer.
   *
   * @param request  incoming HTTP request
   * @param response outcoming HTTP response
   * @param chain    chain of filters
   * @throws IOException      exception thrown when filtering failed
   * @throws ServletException exception thrown when filtering failed
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse httpResponse = (HttpServletResponse) response;
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    httpResponse.setHeader("Access-Control-Allow-Methods", "*");
    httpResponse.setHeader("Access-Control-Allow-Headers", "*");
    httpResponse.setHeader("Access-Control-Allow-Origin", "*");
    httpResponse.setHeader("Access-Control-Max-Age", "*");

    if ("OPTIONS".equalsIgnoreCase(httpRequest.getMethod())) {
      httpResponse.setStatus(200);
    } else {
      chain.doFilter(httpRequest, httpResponse);
    }
  }

}
