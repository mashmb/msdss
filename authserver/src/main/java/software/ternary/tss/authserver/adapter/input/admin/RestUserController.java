package software.ternary.tss.authserver.adapter.input.admin;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.dto.UserDto;
import software.ternary.tss.authserver.core.domain.exception.ValidationException;
import software.ternary.tss.authserver.core.facade.UserFacade;
import software.ternary.tss.authserver.port.input.admin.UserController;
import software.ternary.tss.authserver.port.input.util.ErrorHandler;

/**
 * Implementation of REST user controller.
 *
 * @author TSS
 */
@Slf4j
@RestController
class RestUserController implements UserController {

  private final ErrorHandler errorHandler;
  private final UserFacade userFacade;

  /**
   * RestUserController constructor.
   *
   * @param errorHandler access to error handler
   * @param userFacade   access to user facade
   */
  public RestUserController(ErrorHandler errorHandler, UserFacade userFacade) {
    this.errorHandler = errorHandler;
    this.userFacade = userFacade;
  }

  /**
   * Logout actually authenticated user.
   *
   * @return ResponseEntity HTTP response with operation status
   */
  @Override
  @RequestMapping(path = "/user/logout", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> logout() {
    log.debug("Start of UserController.logout [path = {}]", "/user/logout");
    SecurityContext securityContext = SecurityContextHolder.getContext();
    String username = securityContext.getAuthentication() != null ? securityContext.getAuthentication().getName()
        : null;

    if (username != null) {
      userFacade.logoutUser(username);
    }

    log.debug("End of UserController.logout [path = {}]", "/user/logout");

    return ResponseEntity.status(200).build();
  }

  /**
   * Register new user in application.
   *
   * @param userData new user data
   * @return ResponseEntity HTTP response for registration
   */
  @Override
  @RequestMapping(path = "/user/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> registerUser(@RequestBody UserDto userData) {
    log.debug("Start of UserController.registerUser [path = {}]", "/user/register");

    try {
      userFacade.registerUser(userData);
    } catch (ValidationException ex) {
      log.error("HTTP client errors occurred", ex);
      errorHandler.throwClientError(ex);
    }

    log.debug("End of UserController.registerUser [path = {}]", "/user/register");

    return ResponseEntity.status(201).build();
  }

}
