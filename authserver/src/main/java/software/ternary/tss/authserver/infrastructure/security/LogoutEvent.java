package software.ternary.tss.authserver.infrastructure.security;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Event that represents user logout needs.
 *
 * @author TSS
 */
class LogoutEvent extends ApplicationEvent {

  private static final long serialVersionUID = -1617529838180427730L;

  @Getter
  private final User user;

  /**
   * LogoutEvent constructor.
   *
   * @param source source of event
   * @param user   user that will be logged out
   */
  public LogoutEvent(Object source, User user) {
    super(source);
    this.user = user;
  }

}
