package software.ternary.tss.authserver.core.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data structure for validation error.
 *
 * @author TSS
 */
@Data
@NoArgsConstructor
@JsonDeserialize(builder = ValidationError.ValidationErrorBuilder.class)
public class ValidationError {

  private String message;
  private String field;

  /**
   * ValidationError constructor.
   *
   * @param message validation error message
   * @param field   name of field that failed during validation
   */
  @Builder
  public ValidationError(String message, String field) {
    this.message = message;
    this.field = field;
  }

  /**
   * Validation error JSON builder.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class ValidationErrorBuilder {

  }

}
