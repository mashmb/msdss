package software.ternary.tss.authserver.port.input.util;

import software.ternary.tss.authserver.core.domain.exception.ValidationException;

/**
 * Definition of input error handler.
 *
 * @author TSS
 */
public interface ErrorHandler {

  /**
   * Throw HTTP client error on validation exception.
   *
   * @param ex validation exception
   */
  void throwClientError(ValidationException ex);

}
