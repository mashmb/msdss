package software.ternary.tss.authserver.core.service;

import java.util.List;

import software.ternary.tss.authserver.core.domain.dto.ValidationError;
import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Definition of business logic in users area.
 *
 * @author TSS
 */
public interface UserService {

  /**
   * Add new user to application.
   *
   * @param user new user data
   * @return User user data afer addition
   */
  User addUser(User user);

  /**
   * Get user by email address.
   *
   * @param email email address
   * @return User user for given email address
   */
  User getByEmail(String email);

  /**
   * Logout user from application.
   *
   * @param user user to logout
   */
  void logoutUser(User user);

  /**
   * Validate user data.
   *
   * @param user user data to validate
   * @return List list of validation errors
   */
  List<ValidationError> validateUserData(User user);

  /**
   * Validate user role.
   *
   * @param userRole name of user role
   * @return ValidationError validation error
   */
  ValidationError validateUserRole(String userRole);

}
