package software.ternary.tss.authserver.infrastructure.security;

import java.util.LinkedList;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.core.facade.UserFacade;

/**
 * Implementation of user service for security layer.
 *
 * @author TSS
 */
@Slf4j
@Service
class SecurityUserService implements UserDetailsService {

  private final UserFacade userFacade;

  /**
   * SecurityUserService constructor.
   *
   * @param userFacade access to user facade
   */
  public SecurityUserService(UserFacade userFacade) {
    this.userFacade = userFacade;
  }

  /**
   * Load internal user data by username.
   *
   * @param username user username
   * @return User internal user data
   */
  public User loadSystemUserByUsername(String username) {
    log.debug("Loading system user by username [username = {}]", username);
    User systemUser = userFacade.getUser(username);
    log.debug("System user loaded by username [usrId = {}]", systemUser != null ? systemUser.getUsrId() : null);

    return systemUser;
  }

  /**
   * Load external user data by username.
   *
   * @param username user username
   * @return UserDetails user data in external data structure
   */
  @Override
  public UserDetails loadUserByUsername(String username) {
    log.debug("Loading external user by username [username = {}]", username);
    User systemUser = loadSystemUserByUsername(username);
    JwtUser externalUser = null;

    if (systemUser != null) {
      List<SimpleGrantedAuthority> authorities = new LinkedList<>();

      if (systemUser.getRole() != null) {
        authorities.add(new SimpleGrantedAuthority(systemUser.getRole().name()));
      }

      externalUser = new JwtUser(systemUser.getEmail(), systemUser.getPassword(), authorities,
          systemUser.getFirstName(), systemUser.getLastName());
    }

    log.debug("External user loaded by username [username = {}]",
        externalUser != null ? externalUser.getUsername() : null);

    return externalUser;
  }

}
