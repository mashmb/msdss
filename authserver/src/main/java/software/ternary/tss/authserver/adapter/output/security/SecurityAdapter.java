package software.ternary.tss.authserver.adapter.output.security;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.infrastructure.security.EventPublisher;
import software.ternary.tss.authserver.port.output.security.SecurityPort;

/**
 * Implementation of security output actions.
 *
 * @author TSS
 */
@Slf4j
@Component
class SecurityAdapter implements SecurityPort {

  private final EventPublisher eventPublisher;

  /**
   * SecurityAdapter constructor.
   *
   * @param eventPublisher access to application event publisher
   */
  public SecurityAdapter(EventPublisher eventPublisher) {
    this.eventPublisher = eventPublisher;
  }

  /**
   * Logout user.
   *
   * @param user user to logout
   */
  @Override
  public void logoutUser(User user) {
    log.debug("Logging user out [username = {}]", user.getEmail());
    eventPublisher.publishLogoutEvent(user);
    log.debug("User logged out [username = {}]", user.getEmail());
  }

}
