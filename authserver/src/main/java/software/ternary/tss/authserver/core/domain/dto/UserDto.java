package software.ternary.tss.authserver.core.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User representation out of the application.
 *
 * @author TSS
 */
@Data
@NoArgsConstructor
@JsonDeserialize(builder = UserDto.UserDtoBuilder.class)
public class UserDto {

  private String email;
  private String password;
  private String role;
  private String firstName;
  private String lastName;

  /**
   * UserDto constructor.
   *
   * @param email     user email address
   * @param password  user password
   * @param role      user role
   * @param firstName user first name
   * @param lastName  user last name
   */
  @Builder
  public UserDto(String email, String password, String role, String firstName, String lastName) {
    this.email = email;
    this.password = password;
    this.role = role;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * User JSON builder.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class UserDtoBuilder {

  }

}
