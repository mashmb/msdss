package software.ternary.tss.authserver.adapter.input.util;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import software.ternary.tss.authserver.core.domain.exception.ValidationException;
import software.ternary.tss.authserver.port.input.util.ErrorHandler;
import software.ternary.tss.seclib.dto.ApiExceptionDetailsDto;
import software.ternary.tss.seclib.exception.ApiException;

/**
 * Implementation of REST API error handler.
 *
 * @author TSS
 */
@Component
class RestErrorHandler implements ErrorHandler {

  /**
   * Throw HTTP client error on validation exception.
   *
   * @param ex validation exception
   */
  @Override
  public void throwClientError(ValidationException ex) {
    List<ApiExceptionDetailsDto> details = new LinkedList<>();
    ex.getErrors().forEach(
        err -> details.add(ApiExceptionDetailsDto.builder().message(err.getMessage()).field(err.getField()).build()));
    throw new ApiException(400, ex.getMessage(), details);
  }

}
