package software.ternary.tss.authserver.core.domain.exception;

import java.util.List;

import lombok.Getter;
import software.ternary.tss.authserver.core.domain.dto.ValidationError;

/**
 * Exception used to communicate validation errors.
 *
 * @author TSS
 */
public class ValidationException extends RuntimeException {

  private static final long serialVersionUID = -620829407337475239L;

  @Getter
  private final List<ValidationError> errors;

  /**
   * ValidationException constructor.
   *
   * @param message error message
   * @param errors  detailed validation errors
   */
  public ValidationException(String message, List<ValidationError> errors) {
    super(message);
    this.errors = errors;
  }

}
