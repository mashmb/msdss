package software.ternary.tss.authserver.adapter.output.admin;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.port.output.admin.UserRepository;

/**
 * Database implementation of user repository.
 *
 * @author TSS
 */
@Slf4j
@Repository
@Profile("db")
class DbUserRepository implements UserRepository {

  private final EntityManager entityManager;
  private final CrudUserRepository repo;

  /**
   * DbUserRepository constructor.
   * 
   * @param entityManager access to entity manager
   * @param repo          access to CRUD user repository
   */
  public DbUserRepository(EntityManager entityManager, CrudUserRepository repo) {
    this.entityManager = entityManager;
    this.repo = repo;
  }

  /**
   * Find user by email address.
   *
   * @param email email address
   * @return User user found for given email address
   */
  @Override
  public User findFirtByEmail(String email) {
    log.debug("Start of UserRepository.findFirtByEmail [email = {}]", email);
    String query = "select usr from User usr where usr.email = :email";
    User user;

    try {
      user = entityManager.createQuery(query, User.class).setParameter("email", email).getSingleResult();
    } catch (NoResultException nre) {
      user = null;
    }

    log.debug("End of UserRepository.findFirtByEmail [usrId = {}]", user != null ? user.getUsrId() : null);

    return user;
  }

  /**
   * Save user data.
   * 
   * @param user user data to save
   * @return User user data after save
   */
  @Override
  public User save(User user) {
    log.debug("Start of UserRepository.save [usrId = {}]", user.getUsrId());
    user = repo.save(user);
    log.debug("End of UserRepository.save [usrId = {}]", user.getUsrId());

    return user;
  }

  /**
   * CRUD user repository.
   */
  @Profile("db")
  private interface CrudUserRepository extends CrudRepository<User, Integer> {

  }

}
