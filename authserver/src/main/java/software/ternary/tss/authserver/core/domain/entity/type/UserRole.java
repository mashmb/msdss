package software.ternary.tss.authserver.core.domain.entity.type;

/**
 * Definition of available user roles.
 *
 * @author TSS
 */
public enum UserRole {

  ADMIN

}
