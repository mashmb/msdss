package software.ternary.tss.authserver.adapter.input.sample;

import java.time.OffsetDateTime;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.dto.HelloMessageDto;
import software.ternary.tss.authserver.port.input.sample.HelloWorldController;
import software.ternary.tss.seclib.util.JwtUtils;

/**
 * Rest implementation of hello world controller.
 *
 * @author TSS
 */
@Slf4j
@RestController
class RestHelloWorldController implements HelloWorldController {

  /**
   * Message for authorized user.
   *
   * @return ResponseEntity HTTP response with message
   */
  @Override
  @RequestMapping(path = "/hello/private", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloMessageDto> getPrivateMessage() {
    log.debug("Start of HelloWorldController.getPrivateMessage [path = {}]", "/hello/private");
    HelloMessageDto dto = HelloMessageDto.builder().message("Hello " + JwtUtils.getPrincipalLogin() + "!")
        .timestamp(OffsetDateTime.now()).build();
    log.debug("End of HelloWorldController.getPrivateMessage [path = {}]", "/hello/private");

    return ResponseEntity.status(200).body(dto);
  }

  /**
   * Message for authorized user with permission.
   *
   * @return ResponseEntity HTTP response with message
   */
  @Override
  @PreAuthorize("hasAuthority('ADMIN')")
  @RequestMapping(path = "/hello/private/secured", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloMessageDto> getPrivateSecuredMessage() {
    log.debug("Start of HelloWorldController.getPrivateSecuredMessage [path = {}]", "/hello/private/secured");
    HelloMessageDto dto = HelloMessageDto.builder()
        .message("Hello " + JwtUtils.getPrincipalLogin() + "! You are ADMIN!").timestamp(OffsetDateTime.now()).build();
    log.debug("End of HelloWorldController.getPrivateSecuredMessage [path = {}]", "/hello/private/secured");

    return ResponseEntity.status(200).body(dto);
  }

  /**
   * Message for any user (guests also).
   *
   * @return ResponseEntity HTTP response with message
   */
  @Override
  @RequestMapping(path = "/hello", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloMessageDto> getPublicMessage() {
    log.debug("Start of HelloWorldController.getPublicMessage [path = {}]", "/hello");
    HelloMessageDto dto = HelloMessageDto.builder().message("Hello Guest!").timestamp(OffsetDateTime.now()).build();
    log.debug("End of HelloWorldController.getPublicMessage [path = {}]", "/hello");

    return ResponseEntity.status(200).body(dto);
  }

}
