package software.ternary.tss.authserver.core.facade.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.dto.UserDto;
import software.ternary.tss.authserver.core.domain.dto.ValidationError;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.core.domain.entity.type.UserRole;
import software.ternary.tss.authserver.core.domain.exception.ValidationException;
import software.ternary.tss.authserver.core.facade.UserFacade;
import software.ternary.tss.authserver.core.service.UserService;

/**
 * Implementation of use cases in users area.
 *
 * @author TSS
 */
@Slf4j
@Component
class DefaultUserFacade implements UserFacade {

  private final UserService userService;

  /**
   * DefaultUserFacade constructor.
   *
   * @param userService access to user service
   */
  public DefaultUserFacade(UserService userService) {
    this.userService = userService;
  }

  /**
   * Get user by email address.
   *
   * @param email user email address
   * @return User user for given email address
   */
  @Override
  public User getUser(String email) {
    log.info("Getting user [email = {}]", email);
    User user = userService.getByEmail(email);
    log.info("User got [usrId = {}]", user != null ? user.getUsrId() : null);

    return user;
  }

  /**
   * Logout user.
   *
   * @param email user username to logout as email
   */
  @Override
  public void logoutUser(String email) {
    log.info("Logging user out [email = {}]", email);
    User user = userService.getByEmail(email);

    if (user != null) {
      userService.logoutUser(user);
    }

    log.info("User logged out [email = {}]", user != null ? user.getEmail() : null);
  }

  /**
   * Register new user into application.
   *
   * @param userData new user data
   */
  @Override
  public void registerUser(UserDto userData) {
    log.info("Registering user [email = {}]", userData.getEmail());
    List<ValidationError> validationErrors = new LinkedList<>();
    ValidationError userRoleError = userService.validateUserRole(userData.getRole());
    UserRole role = null;

    if (userRoleError != null) {
      validationErrors.add(userRoleError);
    } else {
      if (userData.getRole() != null) {
        role = UserRole.valueOf(userData.getRole());
      }
    }

    User user = new User(userData.getEmail(), userData.getPassword(), userData.getFirstName(), userData.getLastName());
    user.setRole(role);
    validationErrors.addAll(userService.validateUserData(user));

    if (!validationErrors.isEmpty()) {
      throw new ValidationException("Validation errors occurred", validationErrors);
    }

    user = userService.addUser(user);
    log.info("User registered [usrId = {}]", user.getUsrId());
  }

}
