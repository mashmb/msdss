package software.ternary.tss.authserver.core.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import software.ternary.tss.authserver.core.domain.dto.ValidationError;
import software.ternary.tss.authserver.core.domain.entity.User;
import software.ternary.tss.authserver.core.domain.entity.type.UserRole;
import software.ternary.tss.authserver.core.service.UserService;
import software.ternary.tss.authserver.port.output.admin.UserRepository;
import software.ternary.tss.authserver.port.output.security.SecurityPort;
import software.ternary.tss.authserver.port.output.util.Encoder;

/**
 * Implementation of business logic in users area.
 *
 * @author TSS
 */
@Slf4j
@Service
class DefaultUserService implements UserService {

  private final Encoder encoder;
  private final UserRepository userRepository;
  private final SecurityPort securityPort;

  /**
   * DefaultUserService constructor.
   *
   * @param encoder        access to encoding features
   * @param userRepository access to user repository
   * @param securityPort   access to security output actions
   */
  public DefaultUserService(Encoder encoder, UserRepository userRepository, SecurityPort securityPort) {
    this.encoder = encoder;
    this.userRepository = userRepository;
    this.securityPort = securityPort;
  }

  /**
   * Add new user to application.
   *
   * @param user new user data
   * @return User user data afer addition
   */
  @Override
  public User addUser(User user) {
    log.info("Adding new user [email = {}]", user.getEmail());
    user.setPassword(encoder.encode(user.getPassword()));
    user = userRepository.save(user);
    log.info("New user added [usrId = {}]", user.getUsrId());

    return user;
  }

  /**
   * Get user by email address.
   *
   * @param email email address
   * @return User user for given email address
   */
  @Override
  public User getByEmail(String email) {
    log.info("Getting user by email [email = {}]", email);
    User user = userRepository.findFirtByEmail(email);
    log.info("User got for email [usrId = {}]", user != null ? user.getUsrId() : null);

    return user;
  }

  /**
   * Logout user from application.
   *
   * @param user user to logout
   */
  @Override
  public void logoutUser(User user) {
    log.info("Logging user out [email = {}]", user.getEmail());
    securityPort.logoutUser(user);
    log.info("User logged out [email = {}]", user.getEmail());
  }

  /**
   * Validate user data.
   *
   * @param user user data to validate
   * @return List list of validation errors
   */
  @Override
  public List<ValidationError> validateUserData(User user) {
    log.info("Validating user data [email = {}]", user.getEmail());
    List<ValidationError> errors = new LinkedList<>();

    if (user.getEmail() == null || user.getEmail().trim().isEmpty()) {
      errors.add(ValidationError.builder().message("Email can not be empty").field("email").build());
    } else {
      User existingUser = userRepository.findFirtByEmail(user.getEmail());

      if (existingUser != null) {
        errors.add(ValidationError.builder().message("User already exists").field("email").build());
      }
    }

    if (user.getPassword() == null || user.getPassword().trim().isEmpty()) {
      errors.add(ValidationError.builder().message("Password is required").field("password").build());
    }

    if (user.getFirstName() == null || user.getFirstName().trim().isEmpty()) {
      errors.add(ValidationError.builder().message("First name is required").field("firstName").build());
    }

    if (user.getLastName() == null || user.getLastName().trim().isEmpty()) {
      errors.add(ValidationError.builder().message("Last name is required").field("lastName").build());
    }

    log.info("User data validated [errors size = {}]", errors.size());

    return errors;
  }

  /**
   * Validate user role.
   *
   * @param userRole name of user role
   * @return ValidationError validation error
   */
  @Override
  public ValidationError validateUserRole(String userRole) {
    log.info("Validating user role [userRole = {}]", userRole);
    ValidationError error = null;

    if (userRole != null && !userRole.trim().isEmpty()) {
      try {
        UserRole.valueOf(userRole);
      } catch (Exception ex) {
        error = ValidationError.builder().message("Unknown user role").field("role").build();
      }
    }

    log.info("User role validated [error = {}]", error != null);

    return error;
  }

}
