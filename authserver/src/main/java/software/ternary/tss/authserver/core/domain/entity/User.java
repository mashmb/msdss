package software.ternary.tss.authserver.core.domain.entity;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import software.ternary.tss.authserver.core.domain.entity.type.UserRole;

/**
 * User representation in application.
 *
 * @author TSS
 */
@Entity
@Table(name = "user_details")
public class User {

  private Integer usrId;
  private String email;
  private String password;
  private UserRole role;
  private String firstName;
  private String lastName;

  /**
   * Private empty Hibernate constructor.
   */
  private User() {
  }

  /**
   * User constructor.
   *
   * @param email     user email address
   * @param password  user super secret password
   * @param firstName user first name
   * @param lastName  user last name
   */
  public User(String email, String password, String firstName, String lastName) {
    this.email = email;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  @Id
  @Column(name = "usr_id", unique = true)
  @SequenceGenerator(name = "usr_id_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_id_seq")
  public Integer getUsrId() {
    return usrId;
  }

  public void setUsrId(Integer usrId) {
    this.usrId = usrId;
  }

  @Basic
  @Column(name = "email", unique = true)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "password")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Basic
  @Column(name = "role")
  @Enumerated(EnumType.STRING)
  public UserRole getRole() {
    return role;
  }

  public void setRole(UserRole role) {
    this.role = role;
  }

  @Basic
  @Column(name = "first_name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @Basic
  @Column(name = "last_name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    User user = (User) obj;

    return usrId.equals(user.usrId) && Objects.equals(email, user.email) && Objects.equals(password, user.password)
        && Objects.equals(role, user.role) && Objects.equals(firstName, user.firstName)
        && Objects.equals(lastName, user.lastName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usrId, email, password, role, firstName, lastName);
  }

}
