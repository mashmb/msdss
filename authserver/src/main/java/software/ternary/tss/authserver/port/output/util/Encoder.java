package software.ternary.tss.authserver.port.output.util;

/**
 * Definition of encoding features.
 *
 * @author TSS
 */
public interface Encoder {

  /**
   * Encode given text by encoder.
   *
   * @param toEncode text to encode by encoder
   * @return String encoded by encoder text
   */
  String encode(String toEncode);

}
