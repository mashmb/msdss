package software.ternary.tss.authserver.core.facade;

import software.ternary.tss.authserver.core.domain.dto.UserDto;
import software.ternary.tss.authserver.core.domain.entity.User;

/**
 * Definition of use cases in users area.
 *
 * @author TSS
 */
public interface UserFacade {

  /**
   * Get user by email address.
   *
   * @param email user email address
   * @return User user for given email address
   */
  User getUser(String email);

  /**
   * Logout user.
   *
   * @param email user username to logout as email
   */
  void logoutUser(String email);

  /**
   * Register new user into application.
   *
   * @param userData new user data
   */
  void registerUser(UserDto userData);

}
