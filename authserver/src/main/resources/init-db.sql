--- Initial MSDSS database script.
---
--- @author TSS
---
--- Definition of tables for JDBC token store.
---
create table if not exists oauth_access_token (
  token_id varchar(500) primary key,
  token bytea not null,
  authentication_id varchar(500) not null,
  user_name varchar(250) not null,
  client_id varchar(250) not null,
  authentication bytea not null,
  refresh_token varchar(500) not null
);
create table if not exists oauth_refresh_token (
  token_id varchar(500) primary key,
  token bytea not null,
  authentication bytea not null
);
---
--- Definition of tables for Java entities.
---
create sequence if not exists usr_id_seq start 1 increment 1;
create table if not exists user_details (
  usr_id integer primary key default nextval('usr_id_seq'),
  email varchar(250) not null,
  password varchar(500) not null,
  role varchar(50) null,
  first_name varchar(250) not null,
  last_name varchar(250) not null
);
