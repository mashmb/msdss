#!/bin/bash
# 
# Application development build script.
# 
# @author TSS

./gradlew clean
./gradlew build
java -jar build/libs/authserver-1.0.0.jar --server.port=8082 --spring.profiles.active=auth-server,in-memory
