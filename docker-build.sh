#!/bin/bash 
#
# Multi module project Docker build script.
#
# @author TSS

docker build -t authserver:dev -f authserver/Dockerfile .
docker build -t ms1:dev -f ms1/Dockerfile .
docker build -t ms2:dev -f ms2/Dockerfile .
